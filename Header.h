#pragma once
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int HashFunctionHorner(const std::string& s, int table_size, const int key) //������
{
    int hash_result = 0;
    //for (int i = 0; s[i] != s.size(); ++i) //������ ������
    for (int i = 0; i != s.size(); ++i)
        hash_result = (key * hash_result + s[i]) % table_size;
    hash_result = (hash_result * 2 + 1) % table_size;
    if (hash_result < 0) //������ ������
    {
        hash_result = hash_result + table_size;
    }
    return hash_result;
}
 
int HashFunctionSum(std::string value, int table_size)
{
    int hash_result = 0;
    for (int i = 0; i != value.size(); ++i)
    {
        hash_result = (hash_result + value[i]) % table_size;
    }
    if (hash_result < 0)
    {
        hash_result = hash_result + table_size;
    }
    return hash_result;
}

struct HashFunction1
{
    int operator()(const std::string& s, int table_size) const
    {
        return HashFunctionSum(s, table_size);
    }
};

struct HashFunction2
{
    int operator() (int num) const //����� �������
    {
        return (int(pow(num + 1, 2)));
    }
};


template <class THash1 = HashFunction1, class THash2 = HashFunction2>
class HashTable
{
public:
    HashTable()
    {
        buffer_size = default_size;
        size = 0;
        size_all_non_nullptr = 0;
        arr = new Node * [buffer_size];
        for (int i = 0; i < buffer_size; ++i)
            arr[i] = nullptr; // ��������� nullptr - �� ���� ���� �������� �����������, � ����� ������ �� ����� ������ �� ���������
    }
    ~HashTable()
    {
        for (int i = 0; i < buffer_size; ++i)
            if (arr[i])
                delete arr[i];
        delete[] arr;
    }

    void ResizeMax()
    {
        int past_buffer_size = buffer_size;
        buffer_size = buffer_size * 2;
        size_all_non_nullptr = 0;
        size = 0;
        Node** arr2 = new Node * [buffer_size];
        for (int i = 0; i < buffer_size; ++i)
            arr2[i] = nullptr;
        std::swap(arr, arr2);
        for (int i = 0; i < past_buffer_size; ++i)
        {
            if (arr2[i] && arr2[i]->state)
                Add(arr2[i]->value); // ��������� �������� � ����� ������
        }
        // �������� ����������� �������
        for (int i = 0; i < past_buffer_size; ++i)
            if (arr2[i])
                delete arr2[i];
        delete[] arr2;
    }

    void ResizeMin()
    {
        int past_buffer_size = buffer_size;
        buffer_size = int(buffer_size * 0.75);
        size_all_non_nullptr = 0;
        size = 0;
        Node** arr2 = new Node * [buffer_size];
        for (int i = 0; i < buffer_size; ++i)
            arr2[i] = nullptr;
        std::swap(arr, arr2);
        for (int i = 0; i < past_buffer_size; ++i)
        {
            if (arr2[i] && arr2[i]->state)
                Add(arr2[i]->value); // ��������� �������� � ����� ������
        }
        // �������� ����������� �������
        for (int i = 0; i < past_buffer_size; ++i)
            if (arr2[i])
                delete arr2[i];
        delete[] arr2;
    }

    void Rehash()
    {
        size_all_non_nullptr = 0;
        size = 0;
        Node** arr2 = new Node * [buffer_size];
        for (int i = 0; i < buffer_size; ++i)
            arr2[i] = nullptr;
        std::swap(arr, arr2);
        for (int i = 0; i < buffer_size; ++i)
        {
            if (arr2[i] && arr2[i]->state)
                Add(arr2[i]->value);
        }
        // �������� ����������� �������
        for (int i = 0; i < buffer_size; ++i)
            if (arr2[i])
                delete arr2[i];
        delete[] arr2;
    }

    int Find(const std::string &value, const THash1& hash1 = THash1(), const THash2& hash2 = THash2())
    {
        int i = 0;
        int h1 = hash1(value, buffer_size); // ��������, ���������� �� ��������� �������
        int h2 = hash2(value, buffer_size, i); // ��������, ������������� �� "���" �� �������
        while (arr[h1] != nullptr && i < buffer_size)
        {
            if (arr[h1]->value == value && arr[h1]->state)
                return h1; // ����� ������� ����
            h2 = hash2(value, buffer_size, i);
            h1 = (h1 + h2) % buffer_size;
            ++i; // ���� � ��� i >=  buffer_size, ������ �� ��� ������ ��������� ��� ������
        }
        return -1;
    }

    bool Remove(const std::string& value, const THash1& hash1 = THash1(), const THash2& hash2 = THash2())
    {
        int i = 0;
        int h1 = hash1(value, buffer_size);
        int h2 = hash2(i);
        while (arr[h1] != nullptr && i < buffer_size)
        {
            if (arr[h1]->value == value && arr[h1]->state)
            {
                arr[h1]->state = false;
                --size;
                    if (size < int(rehash_size_min * buffer_size) && (buffer_size*(0.75) > default_size))
                    {
                        ResizeMin();
                    }
                    else if (size_all_non_nullptr > 2 * size)
                    {
                        Rehash();
                    }
                return true;
            }
            h2 = hash2(i);
            h1 = (h1 + h2) % buffer_size;
            ++i;
        }
        return false;
    }

    bool Add(const std::string& value, const THash1& hash1 = THash1(), const THash2& hash2 = THash2())
    {
        if (size + 1 > int(rehash_size_max * buffer_size))
            ResizeMax();
        else if (size_all_non_nullptr > 2 * size)
            Rehash(); // ���������� �����, ��� ��� ������� ����� deleted-���������
        int i = 0;
        int h1 = hash1(value, buffer_size);
        int h2 = hash2(i);
        int first_deleted = -1; // ���������� ������ ���������� (���������) �������
        while (arr[h1] != nullptr && i < buffer_size)
        {
            if (arr[h1]->value == value && arr[h1]->state)
                return false; // ����� ������� ��� ����, � ������ ��� ������ ��������� ��������
            if (!arr[h1]->state && first_deleted == -1) // ������� ����� ��� ������ ��������
                first_deleted = h1;
            h2 = hash2(i);
            h1 = (h1 + h2) % buffer_size;
            ++i;
        }
        if (first_deleted == -1) // ���� �� ������� ����������� �����, ������� ����� Node
        {
            arr[h1] = new Node(value);
            ++size_all_non_nullptr; // ��� ��� �� ��������� ���� ������, �� �������� ��������, ��� ��� ����� ������ ������
        }
        else 
        {
            arr[first_deleted]->value = value;
            arr[first_deleted]->state = true;
        }
        ++size; // � � ����� ������ �� ��������� ���������� ���������
        return true;
    }

    void Print()
    {
        for (int i = 0; i < buffer_size; i++)
        {
            std::cout << "[" << std::setw(4) << i << "]" << " ";
            if (arr[i] != nullptr)
            {
                std::cout << std::setw(5) << arr[i]->value << " " << "[" << arr[i]->state << "]";;
            }
            std::cout << std::endl;
        }
    }

//private
    static const int default_size = 10; // ��������� ������ ����� �������
    constexpr static const double rehash_size_max = 0.75; // �����������, ��� ������� ���������� ���������� �������
    constexpr static const double rehash_size_min = 0.25;
    struct Node
    {
        std::string value;
        bool state; // ���� �������� ����� state = false, ������ ������� ������� ��� ������ (deleted)
        Node(const std::string& value_) : value(value_), state(true) {}
    };
    Node** arr; // �������������� � ������� ����� �������� ��������� Node*
    int size; // ������� ��������� � ��� ������ � ������� (��� ����� deleted)
    int buffer_size; // ������ ������ �������, ������� ������ �������� ��� �������� ����� �������
    int size_all_non_nullptr; // ������� ��������� � ��� ������ � ������� (� ������ deleted)
};

