﻿#include <iostream>
#include "Header.h"

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    HashTable<> table = HashTable<>();
    char m[] = "13";
    for (int i = 1; i < 9; i++)
    {
        table.Add(m);
        cout << HashFunctionSum(m, 10) << endl;
        cout << HashFunctionSum(m, 20) << endl;
        cout << endl;
        table.Print();
        m[0] = m[1];
        m[1] = i;
    }
    char c[] = "13";
    for (int i = 1; i < 9; i++)
    {
        cout << endl;
        table.Remove(c);
        table.Print();
        c[0] = c[1];
        c[1] = i;
    }
    cout << endl;
}
